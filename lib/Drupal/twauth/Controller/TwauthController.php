<?php
namespace Drupal\twauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\RegisterFormController;
use Drupal\user;
use TwitterOAuth;

require_once '/sites/all/libraries/twitteroauth/twitteroauth.php';

class TwauthController extends ControllerBase
{
  public function CONSUMER_TOKEN() {return $this->config('twauth.settings')->get('twitter_consumer_token');}
  public function CONSUMER_SECRET() {return $this->config('twauth.settings')->get('twitter_consumer_secret');}
  public function REDIRECT_CALLBACK() {return $GLOBALS['base_url'] . "/twauth/redirect";}
  
  //Checks if a user is already registered with this Twitter account
  public function existsUser($tid)
  {
    $sql = "SELECT twauth_tid FROM users WHERE twauth_tid = :tid";
    $result = db_query($sql, array(
      ':tid' => $tid,
    ));
    $strings = $result->fetchAll();
    
    return (bool) count($strings);
  }
  
  //Saves a user
  public function saveUser($details)
  {
    
    $fields = array(
      'name' => $details['username'] . $details['tid'],
      'status' => 1,
      'created' => time(),
    );
    
    //Create user
    $account = entity_create('user', $fields);
    $account->save();
    
    //Rename user
    $account->set('name', $details['username'] . $account->id());
    $account->save();
    
    //Update user Twitter ID with db_update
    $update_tid = db_update('users')->fields(array('twauth_tid' => $details['tid']))->condition('uid', $account->id(), '=')->execute();
    
    return $account;
  }
  
  //Select values from a user based on its Twitter ID
  public function selectUserByTid($field, $tid)
  {
    $sql = "SELECT * FROM users WHERE twauth_tid = :tid LIMIT 1";
    $result = db_query($sql, array(':tid' => $tid))->fetchAssoc();
    return $result[$field];
  }
  
  public function twauthSignin()
  {
    
    //Redirect if user is logged in
    if (user_is_logged_in()) {
      return ControllerBase::redirect('user.login', array());
    }
    //Setup client
    $client = new TwitterOAuth($this->CONSUMER_TOKEN(),$this->CONSUMER_SECRET());
	$client->host = "https://api.twitter.com/1.1/";

	//Get request token
	$request_token = $client->getRequestToken($this->REDIRECT_CALLBACK());
    $auth_url = $client->getAuthorizeUrl($request_token);
	$_SESSION['oauth_token'] = $request_token['oauth_token'];
	$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
    
    $output = array();
    $output['twauth'] = array(
      '#markup' => "<a href='{$auth_url}' id='signInButton'><img src='modules/twauth/sign-in-with-twitter-gray.png'></a>",
    );
    return $output;
  }
  
  public function twauthRedirect()
  {
    
    global $user;
    
	//Setup connection
	if (isset($_SESSION['oauth_token']) && isset($_SESSION['oauth_token_secret'])) {
	  $client = new TwitterOAuth($this->CONSUMER_TOKEN(),$this->CONSUMER_SECRET(),$_SESSION['oauth_token'],$_SESSION['oauth_token_secret']);
	}
    else {
     return ControllerBase::redirect('user.login', array());
    }
    
    //Retrieve access token and authenticate with new token
    $token_credentials = $client->getAccessToken($_REQUEST['oauth_verifier']);
	$client = new TwitterOAuth($this->CONSUMER_TOKEN(), $this->CONSUMER_SECRET(), $token_credentials['oauth_token'],$token_credentials['oauth_token_secret']);
    
    //Set token credentials for retrieval
	$_SESSION['token'] = $token_credentials;
	unset($_SESSION['oauth_token']);
	unset($_SESSION['oauth_token_secret']);
    
    //Retrieve data from Twitter
    if ($account = $client->get('account/verify_credentials')) {
      //Register if not already registered
      if (!$this->existsUser($account->id)) {
        $user_details = array(
          'username' => $account->screen_name,
          'tid' => $account->id,
        );
        $local_user = $this->saveUser($user_details);
      } else {
        $local_user = user_load($this->selectUserByTid('uid', $account['id']));
      }
      //Login user as local user
      user_login_finalize($local_user);
      
      return ControllerBase::redirect('<front>', array());
    }
    
    //Otherwise, output error message
    $output = array();
    $output['twauth'] = array(
      '#markup' => 'An error occurred. Please try signing in again.',
    );
    return $output;
  }
  
}
?>