<?php

namespace Drupal\twauth;

use Drupal\Core\Form\ConfigFormBase;

/**
* Form constructor for the administrator configuration of the Twitter Authentication module.
*/

class TwauthSettingsForm extends ConfigFormBase
{
  
  public function getFormId()
  {
    return 'twauth_configure';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state)
  {
    $config = $this->configFactory->get('twauth.settings');
    
    $form = array();
    
    $form['twauth'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
    );
    
    $form['twauth']['twitter_consumer_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter consumer token'),
      '#default_value' => $config->get('twitter_consumer_token'),
    );
    
    
    $form['twauth']['twitter_consumer_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter consumer secret'),
      '#default_value' => $config->get('twitter_consumer_secret'),
    );
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $this->configFactory->get('twauth.settings')->set('twitter_consumer_token', $form_state['values']['twitter_consumer_token'])->set('twitter_consumer_secret', $form_state['values']['twitter_consumer_secret'])->save();
    
    parent::submitForm($form, $form_state);
  }
  
}
